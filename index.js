'use strict';

let fs = require("fs")

console.log("Need firebase-add-credentials.json and user-credentials.json")

let firebaseAppCredentialsRaw = fs.readFileSync('firebase-app-credentials.json');
let firebaseAppCredentials = JSON.parse(firebaseAppCredentialsRaw);
console.log("firebaseAppCredentials", firebaseAppCredentials);

let userCredentialsRaw = fs.readFileSync('user-credentials.json');
let userCredentials = JSON.parse(userCredentialsRaw);
console.log("userCredentials", userCredentials);

let firebase = require("firebase");
let firebaseApp = firebase.initializeApp(firebaseAppCredentials);

firebaseApp.auth().signInWithEmailAndPassword(userCredentials.email, userCredentials.password)
  .then((userCredential) => {
    console.log("Signed in")
    var user = userCredential.user;
    user.getIdToken().then(function(idToken) {
        // Send token to your backend via HTTPS
        // ...
        console.log("Retrieved IdToken:", idToken)
      }).catch(function(error) {
        console.log("Retrieve Token Error Code:", error.code)
        console.log("Retrieve Token Error Message:", error.message)    
      });
    // ...
  })
  .catch((error) => {
      console.log("Sign In Error Code:", error.code)
      console.log("Sign In Error Message:", error.message)    
});


// 
// firebase.

