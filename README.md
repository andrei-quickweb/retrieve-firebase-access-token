# Usage

```
git clone https://gitlab.com/andrei-quickweb/retrieve-firebase-access-token.git
cd retrieve-firebase-access-token
yarn install
```

Now create `firebase-app-credentials.json` and `user-credentials.json` (example files are provided).

```
node index.js
```

